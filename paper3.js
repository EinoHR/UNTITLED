var pathOptions = {
    strokeColor: 'black',
    strokeWidth: 5,
}

var path = new Path(pathOptions);
var compound = new CompoundPath(pathOptions);
compound.addChild(path);

path.add(Point.random()*view.viewSize);

onFrame = function() {
    var newPoint = getNewPoint(path.lastSegment.point);
    var testpath = new Path([path.lastSegment.point, newPoint]);
    if (compound.getCrossings(testpath).length != 0) {
        console.log(path.getCrossings(testpath));
        path = new Path(pathOptions);
        compound.addChild(path);
    }
    path.add(newPoint)
    path.smooth();
}

function getNewPoint(oldPoint) {
    var newPoint = oldPoint + (Point.random() - .5) * (Math.random() * 200);

    if (newPoint.x > view.viewSize.width) {
        return getNewPoint(oldPoint);
    }
    else if (newPoint.x < 0) {
        return getNewPoint(oldPoint);
    }
    else if (newPoint.y > view.viewSize.height) {
        return getNewPoint(oldPoint);
    }
    else if (newPoint.y < 0) {
        return getNewPoint(oldPoint);
    }

    return newPoint;
}